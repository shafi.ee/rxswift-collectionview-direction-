//
//  ThirdViewController.swift
//  collectionView
//
//  Created by Amin Shafiee on 9/22/1397 AP.
//  Copyright © 1397 amin. All rights reserved.
//

import Foundation
import UIKit
import RxCocoa
import RxSwift

class ThirdViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var imageURLs = PublishSubject<[String]>()
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
    }
    
    private func setupCollectionView() {
        collectionView.delegate = self
        collectionView.register(UINib(nibName: "CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CollectionViewCell")
        
        imageURLs.bind(to: collectionView.rx.items(cellIdentifier: "CollectionViewCell", cellType: CollectionViewCell.self)) {  (row,image,cell) in
            
            cell.imageView.image = UIImage(named: image)
            cell.backgroundColor = .red
        }.disposed(by: disposeBag)
    }
    
    
}

//insetForSectionAtIndex
extension ThirdViewController:UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 250, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
}
