//
//  ViewController.swift
//  collectionView
//
//  Created by Amin Shafiee on 9/22/1397 AP.
//  Copyright © 1397 amin. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ViewController: UIViewController {

    @IBOutlet weak var thirdCollectionView: UIView!
    @IBOutlet weak var secondCollectionView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    private let viewModel = CollectionViewModel()
    let disposeBag = DisposeBag()
    var imageURLs = PublishSubject<[String]>()
    
    
    private lazy var SecondVC: SecondViewController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "SecondViewController") as! SecondViewController
        self.add(asChildViewController: viewController, to: secondCollectionView)
        return viewController
    }()
    
    private lazy var ThirdVC: ThirdViewController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "ThirdViewController") as! ThirdViewController
        self.add(asChildViewController: viewController, to: thirdCollectionView)
        return viewController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupCollectionView()
        viewModel.items.bind(to: ThirdVC.imageURLs).disposed(by: disposeBag)
        viewModel.items.bind(to: imageURLs).disposed(by: disposeBag)
        viewModel.getAdvertisments()
        
    }
    
    private func setupCollectionView() {
        collectionView.delegate = self
        collectionView.register(UINib(nibName: "CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CollectionViewCell")
        
        imageURLs.bind(to: collectionView.rx.items(cellIdentifier: "CollectionViewCell", cellType: CollectionViewCell.self)) {  (row,image,cell) in
            
            cell.imageView.image = UIImage(named: image)
            cell.backgroundColor = UIColor(red: 13/255.0, green: 152/255.0, blue: 10/255.0, alpha: 1)
            }.disposed(by: disposeBag)
    }
}

//insetForSectionAtIndex
extension ViewController:UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 250, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
}



extension UIViewController {
    public func add(asChildViewController viewController: UIViewController,to parentView:UIView) {
        // Add Child View Controller
        addChild(viewController)
        
        // Add Child View as Subview
        parentView.addSubview(viewController.view)
        
        // Configure Child View
        viewController.view.frame = parentView.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        // Notify Child View Controller
        viewController.didMove(toParent: self)
    }
}
