//
//  CollectionViewCell.swift
//  collectionView
//
//  Created by Amin Shafiee on 9/22/1397 AP.
//  Copyright © 1397 amin. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
