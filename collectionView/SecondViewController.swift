//
//  secondViewController.swift
//  collectionView
//
//  Created by Amin Shafiee on 9/22/1397 AP.
//  Copyright © 1397 amin. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()

        collectionView.register(
            UINib(nibName: "CollectionViewCell", bundle: nil),
            forCellWithReuseIdentifier: "CollectionViewCell")
        collectionView.delegate = self
        collectionView.dataSource = self
    }

}

extension SecondViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath as IndexPath) as! CollectionViewCell
        if indexPath.row == 0 {
            cell.imageView.image = UIImage(named: "img1")
        } else if indexPath.row == 1 {
            cell.imageView.image = UIImage(named: "img2")
        } else {
            cell.imageView.image = UIImage(named: "img3")
        }
        
        cell.backgroundColor = UIColor(red: 13/255.0, green: 152/255.0, blue: 10/255.0, alpha: 1)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 250, height: 100)
    }
}

