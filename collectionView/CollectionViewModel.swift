//
//  CollectionViewModel.swift
//  collectionView
//
//  Created by Amin Shafiee on 9/22/1397 AP.
//  Copyright © 1397 amin. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class CollectionViewModel {
    
    let items = PublishSubject<[String]>()
    
    func getAdvertisments() {
        
        var sections : [String] = []
        sections.append("img1")
        sections.append("img2")
        sections.append("img3")
        
        items.onNext(sections)
    }
}
